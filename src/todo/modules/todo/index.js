import axios from 'axios';

const state = {
    todos: []
}

const mutations = {
    UPDATE_TODO_ITEMS (state, payload) {
        state.todos = payload;
    },
    ADD_TODO_ITEM (state, payload) {
        console.log(payload)
        state.todos.push(payload);
    },
    DELETE_TODO_ITEM (state, payload) {
        const index = state.todos.indexOf(payload);
        state.todos.splice(index, 1);
    },
    ADD_ITEM (state, payload) {
        if (state.todos[payload[0]].items) {
            state.todos[payload[0]].items.push(payload[1])
        } else {
            state.todos[payload[0]].items = [payload[1]];
        }
    },
    UPDATE_ITEM (state, payload) {
        const todoIndex = state.todos.indexOf(payload[0]);
        state.todos[todoIndex].items[payload[1]] = payload[2];
    },
    DELETE_ITEM (state, payload) {
        const todoIndex = state.todos.indexOf(payload[0]);
        state.todos[todoIndex].items.splice(payload[1], 1);
    }
}

const actions = {
    getTodos ({ commit }) {
        axios.get(`http://127.0.0.1/api/todos`).then((response) => {
            commit('UPDATE_TODO_ITEMS', response.data)
        });
    },
    createTodo ({ commit }, newTodo) {
        axios.post(`http://127.0.0.1/api/todo/store`, {
            todo: {
                name: newTodo
            }
        }).then((response) => {
            commit('ADD_TODO_ITEM', response.data)
        });
    },
    deleteTodo ({ commit }, todo) {
        axios.delete(`http://127.0.0.1/api/todo/` + todo.id).then(() => {
            commit('DELETE_TODO_ITEM', todo)
        });
    },
    createItem ({ commit }, item) {
        axios.post('http://127.0.0.1/api/' + item[1].id + '/item/store', {
            item: {
                name: item[0]
            }
        }).then((response) => {
            commit('ADD_ITEM', [item[2], response.data])
        });
    },
    updateItem ({ commit }, item) {
        console.log(item)
        axios.put(`http://127.0.0.1/api/item/` + item[1].id, {
            item: {
                name: item[1].name,
                status: item[2]
            }
        }).then((response) => {
            commit('UPDATE_ITEM', [item[0], item[3], response.data])
        });
    },
    deleteItem ({ commit }, item) {
        axios.delete(`http://127.0.0.1/api/item/` + item[1].id).then(() => {
            commit('DELETE_ITEM', [item[0], item[2]])
        });
    }
}

const getters = {
    todos: state => state.todos
}

const todoModule = {
    state,
    mutations,
    actions,
    getters
}

export default todoModule;
