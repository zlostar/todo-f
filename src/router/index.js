import { createRouter, createWebHashHistory } from 'vue-router'
import TodoList from "@/components/todo/TodoList.vue";

const routes = [
  {
    path: '/todos',
    component: TodoList
  },
  {
    path: '/',
    redirect: '/todos'
  },
]
const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
