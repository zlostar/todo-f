import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import todo from './todo'
import './../node_modules/bulma/css/bulma.css'

createApp(App).use(todo).use(router).mount('#app')
