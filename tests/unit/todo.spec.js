import todo from '@/todo/modules/todo/index';

describe("todo", () => {
  it("adds a todo to the state", () => {
    const t = {name: 'fdsfds', updated_at: '2023-08-15T07:05:57.000000Z', created_at: '2023-08-15T07:05:57.000000Z', id: 11}
    const state = {
      todos: []
    }

    todo.mutations.ADD_TODO_ITEM(state, t)

    expect(state.todos)
        .toEqual([{name: 'fdsfds', updated_at: '2023-08-15T07:05:57.000000Z', created_at: '2023-08-15T07:05:57.000000Z', id: 11}])
  })

  it("updates the todos in the state", () => {
    const t = {name: 'fdsfds', updated_at: '2023-08-15T07:05:57.000000Z', created_at: '2023-08-15T07:05:57.000000Z', id: 11}
    const state = {
      todos: []
    }

    todo.mutations.UPDATE_TODO_ITEMS(state, [t])

    expect(state.todos)
        .toEqual([{name: 'fdsfds', updated_at: '2023-08-15T07:05:57.000000Z', created_at: '2023-08-15T07:05:57.000000Z', id: 11}])
  })

  it("deletes a todo from the state", () => {
    const t = {name: 'fdsfds', updated_at: '2023-08-15T07:05:57.000000Z', created_at: '2023-08-15T07:05:57.000000Z', id: 11}
    const state = {
      todos: [{name: 'fdsfds', updated_at: '2023-08-15T07:05:57.000000Z', created_at: '2023-08-15T07:05:57.000000Z', id: 11}]
    }

    todo.mutations.DELETE_TODO_ITEM(state, [t])

    expect(state.todos)
        .toEqual([])
  })
})